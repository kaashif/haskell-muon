haskell-muon
============
Muon is a static site generator that can (or will be able to):

- Generate HTML sites from Markdown
- Deploy to a server using rsync, scp, magic, you name it!
- Here's the big feature: it'll also generate files you can put into a
  Gopher hole!

Installing
----------

	$ cabal install muon

Using
-----

	$ muon help

License
-------
See the [LICENSE](/LICENSE) file.
