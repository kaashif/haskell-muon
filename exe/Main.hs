module Main where
import qualified Muon.Main (main)

main :: IO ()
main = Muon.Main.main
