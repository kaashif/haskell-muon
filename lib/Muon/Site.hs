module Muon.Site where
import qualified Shelly as S
import Data.List
import qualified Data.Text as T
import Paths_muon

-- | Copies default site files to p
initialise :: FilePath -> IO ()
initialise p = do
  putStrLn "Initialising"
  dir <- getDataFileName ""
  S.shelly $ S.cp_r (S.fromText . T.pack $ stripR '/' dir) (S.fromText $ T.pack p)
    where stripR c = reverse . dropWhile (==c) . reverse

-- | Builds an existing site
build :: IO ()
build = putStrLn "Building"

-- | Deploys an existing site
deploy :: IO ()
deploy = putStrLn "Deploying"
