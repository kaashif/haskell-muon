{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveDataTypeable #-}
module Muon.Main where
import Control.Monad (void, mapM_)
import System.Process
import System.Environment (getExecutablePath)
import System.Exit
import System.Console.CmdArgs.Implicit
import System.Console.CmdArgs.Explicit (processArgs)
import qualified Muon.Site as Site
import qualified Data.Map as M

data Muon = Help
          | Init { dir :: FilePath }
          | Build
          | Deploy
          | Do { cmds :: [String] }
            deriving (Data, Typeable)

-- | Real main function, used by executable module
main :: IO ()
main = act =<< cmdArgs progMode
                                                                       
progMode = modes [help_ &= auto, init_, build_, deploy_, do_ ] &= summary "muon v0.0.1"
                                                               &= program "muon"
                                                               &= help "Static site generator"
help_ = Help &= help "Show the top-level help message (default)" 
init_ = Init { dir = "." &= argPos 0 &= typDir
             } &= help "Initialise a site in DIR"
build_ = Build &= help "Build a site"
deploy_ = Deploy &= help "Deploy a site" 
do_ = Do { cmds = [] &= args &= typ "COMMANDS"
         } &= help "Meta-command for executing other commands" 

-- | Takes a mode, executes an action
act :: Muon -> IO ()
act Help = runSelf "--help"
act (Init dir) = Site.initialise dir
act Build = Site.build
act Deploy = Site.deploy
act (Do cmds) = do
  if null invalids
  then mapM_ runSelf (cmds)
  else putStrLn ("Invalid commands: " ++ unwords invalids) >> exitFailure
      where invalids = filter (\e -> e `notElem` doables) cmds

-- | Re-runs the program basically as $0 $argstr and waits for it to end
runSelf :: String -> IO ()
runSelf argstr = do
  exe <- getExecutablePath
  let arg = words argstr
  (_, _ ,_, p) <- createProcess $ CreateProcess { cmdspec = RawCommand exe arg
                                                , cwd = Nothing
                                                , env = Nothing
                                                , std_in = Inherit
                                                , std_out = Inherit
                                                , std_err = Inherit
                                                , close_fds = False
                                                , create_group = False
                                                }
  code <- waitForProcess p
  case code of
    ExitSuccess -> return ()
    _ -> exitWith code
  
-- | Modes that can be executed in sequence
doables :: [String]
doables = ["help", "build", "deploy"]
